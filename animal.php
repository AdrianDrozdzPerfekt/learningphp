<?php

/**
* 
*/
class Animal
{
	public $name;
	public $voice;

	public function __construct($name, $voice = '') 
	{
		$this->name = $name;
		$this->voice = $voice;
	}

	public function giveSound() 
	{
		echo $this->voice . PHP_EOL;
	}
}

$dog = new Animal('Burek', 'Bow Wow');
$dog->giveSound();

$snake = new Animal('Alvin', 'SsssSSssSSS');
$snake->giveSound();

$ryba = new Animal('Dory');
$ryba->giveSound();


<?php

/**
* 
*/
class Osoba
{
	//WLASNOCI / CECHY KLASY
	//widocznosc [public, protected, private] $nazwa;
	public $name;
	public $surname;
	public $height;
	public $weight;

	//METODY / CZYNNOSCI KLASY
	//widocznosc [public, protected, private] `function` nazwa() {}

	public function introduce() {
		echo 'Cześć, mam na imię ' . $this->name;
	}

	//$this odnosi się do obiektu z którego został wywołany
}

$o1 = new Osoba(); //tworzenie obiektu/instancji klasy
$o1->name = 'Adrian'; //przypisanie wartosci do wlasnosci
$o1->surname = 'Drozdz'; //przypisanie wartosci do wlasnosci
$o1->height = 173; //przypisanie wartosci do wlasnosci

$o2 = new Osoba(); //tworzenie obiektu/instancji klasy
$o2->name = 'Jan'; //przypisanie wartosci do wlasnosci
$o2->surname = 'Nowak'; //przypisanie wartosci do wlasnosci
$o2->height = 163; //przypisanie wartosci do wlasnosci
$o2->introduce();
var_dump($o1);
var_dump($o2);
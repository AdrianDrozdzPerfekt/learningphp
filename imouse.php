<?php

function __autoload($classname)
{
	include strtolower($classname) . '.php';
}

$mouse = new Mouse();
$mouse->setNumberOfButtons(4)
		->setWeight(160)
		->setSensitivity(1200)
		->setSensorType('laser')
		->setWireless(false);

echo 'Wyswietlam dane myszki zwyklej' . PHP_EOL;
echo 'Liczba przyciskow ' . $mouse->getNumberOfButtons() . PHP_EOL;
echo 'Waga ' . $mouse->getWeight() . PHP_EOL;
echo 'Czulosc ' . $mouse->getSensitivity() . PHP_EOL;
echo 'Sensor ' . $mouse->getSensorType() . PHP_EOL;
echo ($mouse->getWireless()) 
	? 'Bezprzewodowa' . PHP_EOL 
	: 'Przewodowa' . PHP_EOL;


$gmouse = new GamingMouse();
$gmouse->setNumberOfButtons(7)
		->setWeight(190)
		->setSensitivity(400)
		->setSensorType('optical')
		->setRgb(true)
		->setWt(true)
		->setMacro('Ctrl Alt 0 Space 1 Space A Z G')
		->setWireless(true);

echo 'Wyswietlam dane myszki gamingowej' . PHP_EOL;
echo 'Liczba przyciskow ' . $gmouse->getNumberOfButtons() . PHP_EOL;
echo 'Waga ' . $gmouse->getWeight() . PHP_EOL;
echo 'Czulosc ' . $gmouse->getSensitivity() . PHP_EOL;
echo 'Sensor ' . $gmouse->getSensorType() . PHP_EOL;
echo ($gmouse->getWireless()) 
	? 'Bezprzewodowa' . PHP_EOL 
	: 'Przewodowa' . PHP_EOL;
echo ($gmouse->getRgb()) 
	? 'Podswietlana' . PHP_EOL 
	: 'Brak podswietlenia' . PHP_EOL;
echo ($gmouse->getWt()) 
	? 'Zawiera ciezarki w zestawie' . PHP_EOL 
	: 'Brak ciezarkow' . PHP_EOL;

$gmouse->useMacro();
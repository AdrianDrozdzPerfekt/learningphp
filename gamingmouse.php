<?php

/**
* 
*/
class GamingMouse extends Mouse
{
	protected $rgb;
	protected $wt;
	protected $macro;

    /**
     * @return mixed
     */
    public function getRgb()
    {
        return $this->rgb;
    }

    /**
     * @param mixed $rgb
     *
     * @return self
     */
    public function setRgb($rgb)
    {
        $this->rgb = $rgb;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWt()
    {
        return $this->wt;
    }

    /**
     * @param mixed $wt
     *
     * @return self
     */
    public function setWt($wt)
    {
        $this->wt = $wt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMacro()
    {
        return $this->macro;
    }

    /**
     * @param mixed $macro
     *
     * @return self
     */
    public function setMacro($macro)
    {
        $this->macro = $macro;

        return $this;
    }


    public function useMacro()
    {
    	//rozbicie ciagu znakow na tablice wg separatora
    	$keys = explode(' ', $this->macro);
    	//print_r($keys);
    	//dla kazdego elementu z tablicy $keys
    	//wyswietl element jako zmienna $key
    	foreach ($keys as $key) {
    		echo $key;
    		sleep(2);
    	}
    }
}
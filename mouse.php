<?php

/**
* 
*/
class Mouse
{
	
	protected $numberOfButtons;
	protected $weight;
	protected $sensitivity;
	protected $sensorType;
	protected $wireless;

	

	public function click()
	{
		# code...
	} 

	public function scroll()
	{
		# code...
	}

    /**
     * @return mixed
     */
    public function getNumberOfButtons()
    {
        return $this->numberOfButtons;
    }

    /**
     * @param mixed $numberOfButtons
     *
     * @return self
     */
    public function setNumberOfButtons($numberOfButtons)
    {
        $this->numberOfButtons = $numberOfButtons;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     *
     * @return self
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSensitivity()
    {
        return $this->sensitivity;
    }

    /**
     * @param mixed $sensitivity
     *
     * @return self
     */
    public function setSensitivity($sensitivity)
    {
        $this->sensitivity = $sensitivity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSensorType()
    {
        return $this->sensorType;
    }

    /**
     * @param mixed $sensorType
     *
     * @return self
     */
    public function setSensorType($sensorType)
    {
        $this->sensorType = $sensorType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWireless()
    {
        return $this->wireless;
    }

    /**
     * @param mixed $wireless
     *
     * @return self
     */
    public function setWireless($wireless)
    {
        $this->wireless = $wireless;

        return $this;
    }
}
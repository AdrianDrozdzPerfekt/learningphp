<?php

/**
* 
*/
class Letter
{
	
	protected $koperta;
	protected $waga;
	protected $adres;
	protected $wyslany;

    public function __construct()
    {
        $this->wyslany = false;
    }


    /**
     * @return mixed
     */
    public function getKoperta()
    {
        return $this->koperta;
    }

    /**
     * @param mixed $koperta
     *
     * @return self
     */
    public function setKoperta($koperta)
    {
        $this->koperta = $koperta;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWaga()
    {
        return $this->waga;
    }

    /**
     * @param mixed $waga
     *
     * @return self
     */
    public function setWaga($waga)
    {
        $this->waga = $waga;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdres()
    {
        return $this->adres;
    }

    /**
     * @param mixed $adres
     *
     * @return self
     */
    public function setAdres($adres)
    {
        $this->adres = $adres;

        return $this;
    }

    /**
     * @return mixed
     */
    public function isWyslany()
    {
        return $this->wyslany;
    }

    /**
     * @param mixed $wyslany
     *
     * @return self
     */
    public function setWyslany($wyslany)
    {
        $this->wyslany = $wyslany;

        return $this;
    }
}
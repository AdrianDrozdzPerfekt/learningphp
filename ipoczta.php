<?php

function __autoload($classname)
{
	include strtolower($classname) . '.php';
}

$lz = new LetterZwykly();
$lz->setKoperta('C6')
	->setWaga(85)
	->setAdres('Adres');

$ll = new LetterLotniczy();
$ll->setKoperta('A4')
	->setWaga(18)
	->setAdres('Address');

$lp = new LetterPolecony();
$lp->setKoperta('C4')
	->setWaga(54)
	->setAdres('Adresik');

$la = 'Adrian';

$p = new Poczta();
$p->przyjmijList($lz);
//$p->wyslijList($lz);
$p->przyjmijList($ll);
//$p->wyslijList($ll);
$p->przyjmijList($lp);
//$p->wyslijList($lp);
$p->wyslijListy();

echo ($lz->isWyslany()) ? 'LZ wyslany' : 'LZ nie wyslany';
echo ($ll->isWyslany()) ? 'LL wyslany' : 'LL nie wyslany';
echo ($lp->isWyslany()) ? 'LP wyslany' : 'LP nie wyslany';
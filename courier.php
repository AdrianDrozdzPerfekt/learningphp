<?php

/**
* 
*/
class Courier
{
	
	public $name;
	public $price;

	/*
	- konstruktor wywoływany jest zawsze przy tworzeniu obiektu
	- do argumentow konstruktora mozna przekazac dane ktore bede przypisane do
	wlasnosci obiektu (tak jak tu $name i $price)
	- argumenty moga miec domyslne wartosci na wypadek nie podania ich
	ale jesli jakis argument ma byc obowiazkowy to nie dodajemy wartosci domyslnej
	- argumenty z domyslnymi wartosciamy zawsze na koncu
	*/
	public function __construct($name, $price = 0)
	{
		$this->name = $name;
		$this->price = $price;
	}

	public function send()
	{
		echo 'Jestem ' . $this->name 
		. '. Wyslanie paczki kosztuje ' 
		. $this->price . PHP_EOL;
	}
	
}

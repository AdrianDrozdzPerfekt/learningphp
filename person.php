<?php

/**
* 
*/
class Person
{
	
	private $name;
	private $surname;
	private $password;
	private $email;
	private $nice;

	function __construct($name, $surname)
	{
		$this->name = $name;
		$this->surname = $surname;
	}

	//setter musi przyjac $argument do
	//ustawienia danej własności
	//!!plynny interfejs setter zwraca ten sam obiekt z ktorego
	//!!zostal wywolany `return $this;`
	public function setEmail($email) 
	{
		$this->email = $email;
		return $this;
	}

	public function getEmail() 
	{
		return $this->email;
	}
	
	public function setPassword($password = null) 
	{
		if ($password == null) {
			throw new Exception('Proszę podać hasło');
		}

		//trim() -> php.net.pl
		if (trim($password) == '') { 
			throw new Exception('Hasło nie może być spacjami');
		}

		//strlen() -> php.net.pl
		if (strlen(trim($password)) < 6) { 
			throw new Exception('Hasło musi mieć 6 znaków');
		}

		$this->password = sha1($password);
		return $this;
	}

	public function getPassword() 
	{
		return $this->password;
	}

	//nie dodajemy setterow jesli cos ustawiamy w konstruktorze
	public function getName() 
	{
		return $this->name;
	}

	//nie dodajemy setterow jesli cos ustawiamy w konstruktorze
	public function getSurname()
	{
		return $this->surname;
	}

	//Hello, Jan Nowak!
	public function welcome()
	{
		return 'Hello, ' . $this->name . ' ' . $this->surname . '!' . PHP_EOL;
	}

	//setters and getters for boolean values (true/false) setProperty() and isProperty()
	public function setNice($nice = true) 
	{
		$this->nice = $nice;
		return $this;
	}

	public function isNice() 
	{
		return $this->nice;
	}

}

//try { //instrukcje } catch (Exception $e) { //obsluga wyjatku }
//try catch -> php.net.pl
try {
	$p1 = new Person('Jan', 'Nowak');
	$p1->setEmail('esp@com.pl')
		->setPassword('                 ');
} catch (Exception $e) {
	echo 'Nie udalo wykonac kodu z powodu bledu.' . PHP_EOL;
	echo 'BLAD: ' . $e->getMessage() . PHP_EOL;
}
echo $p1->welcome();
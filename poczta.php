<?php

/**
* 
*/
class Poczta
{
	
	private $lettersPackage = [];

	public function przyjmijList(Letter $list)
	{
		//get_class($obiekt) zwraca nazwę klasy obiektu
		echo 'Przyjmuje ' . get_class($list) . PHP_EOL;
		$this->addToLettersPackage($list);
		echo 'Dziekujemy! To wszystko!' . PHP_EOL;
	}

	public function wyslijList(Letter $list)
	{
		echo 'Wysylam ' . get_class($list) . PHP_EOL;
		$list->setWyslany(true);
		echo 'Dziekujemy! List zostal wyslany!' . PHP_EOL;
	}

	public function wyslijListy()
	{
		print_r($this->lettersPackage);
		//petla tablicowa
		foreach ($this->lettersPackage as $indeks => $letter) {
			$letter->setWyslany(true);
			$this->removeFromLettersPackage($indeks);
		}
		print_r($this->lettersPackage);
	}


    /**
     * @return mixed
     */
    public function getLettersPackage()
    {
        return $this->lettersPackage;
    }

    /**
     * @param mixed $lettersPackage
     *
     * @return self
     */
    public function setLettersPackage($lettersPackage)
    {
        $this->lettersPackage = $lettersPackage;

        return $this;
    }

    /**
     * @param Letter $letter
     *
     * @return self
     */
    public function addToLettersPackage(Letter $letter)
    {
        //dodanie elementu do tablicy
        $this->lettersPackage[] = $letter;

        return $this;
    }

    /**
     * @param int $index
     *
     * @return self
     */
    public function removeFromLettersPackage($index)
    {
        //usuwanie elementu z tablicy o danym indeksie
        unset($this->lettersPackage[$index]);

        return $this;
    }

    /**
     * @param int $index
     *
     * @return self
     */
    public function isInLettersPackage($index)
    {
        //usuwanie elementu z tablicy o danym indeksie
        retrun (array_key_exists($index, $this->lettersPackage));
    }
}
<?php

/**
* 
*/
class SimpleClass
{
	public $name;
	private $home_country;

	function __construct()
	{
		echo 'Hello World!';
	}


	public function setHomeCountry($home_country)
	{
		$this->home_country = $home_country;
	}

	public function getHomeCountry()
	{
		return $this->home_country;
	}

}

$obj = new SimpleClass();
$obj->name = 'Długopis'; //przypisanie wartosci
echo $obj->name; //wyswietlenie/pobranie wartosci
$obj->setHomeCountry('Polska');
echo $obj->getHomeCountry();

$obj2 = new SimpleClass();
echo $obj2->getHomeCountry();

$obj3 = new SimpleClass();
<?php

class PublicPerson
{
	public $name;

	public function __construct($name) {
		$this->name = $name;
	}
}

class PrivatePerson
{
	private $name;
	private $lastname;

	function __construct($name)	{
		$this->name = $name;
	}

	//private wlasnosci zwracamy przez gettera
	public function getName() {
		return $this->name;
	}

	//SETTER AND GETTER EXAMPLE 
	public function getLastname() {
		return $this->lastname;
	}

	public function setLastname($lastname) {
		$this->lastname = $lastname;
	}
}

class ProtectedPerson
{
	protected $name;

	function __construct($name) {
		$this->name = $name;
	}

	//protected wlasnosci zwracamy przez gettera
	public function getName() {
		return $this->name;
	}
}

echo 'OSOBA PUBLIC' . PHP_EOL;
$public = new PublicPerson('Janek');
echo $public->name . PHP_EOL;

echo 'OSOBA PRIVATE' . PHP_EOL;
$private = new PrivatePerson('Janek');
echo $private->getName() . PHP_EOL;
$private->setLastname('Nowak');
echo $private->getName() . ' ' . $private->getLastname() . PHP_EOL;

echo 'OSOBA PROTECTED' . PHP_EOL;
$protected = new ProtectedPerson('Janek');
echo $protected->getName() . PHP_EOL;
